﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamCodeCamp
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        private void BtnReg_Clicked(object sender, EventArgs e)
        {
            DisplayAlert("Registration", "Successful", "Ok");
        }
    }
}
